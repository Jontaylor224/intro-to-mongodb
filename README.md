So far, the only kind of **persistent storage** you've used in your Node/Express applications is the file system, which you used for storing uploaded photos in KenzieGram. Our other Node projects keep all their data in memory on the server, and everything disappears forever when the server is shut down (that is, when you click Ctrl-C on the terminal window where you were running the node application).

To build real applications, we need a a persistent, durable way to store our applications' data. This type of data store is usually called a **database**. In this course, we will look at two different technologies for doing this: **Relational Databases**(accessed via the Structured Query Language, **SQL**) and **Document Databases**(aka **NoSQL**). Both types will support storing and retrieving various types of data via the **CRUD** operations we learned about previously (Create, Read, Update, Delete) and generally also provide ways to search over the stored data.

Currently, the two most widely used relational databases are **Oracle**(expensive) and **MySQL**(free). The most widely used document database is called **MongoDB**. If you are curious, you can check out [https://db-engines.com/en/ranking](https://db-engines.com/en/ranking) for a look at the relative popularity of various database systems. In this course we will get hands on with both MySQL and MongoDB to give you experience with one market leading database from each category.

We will start by looking at **MongoDB**.

In order to introduce you to MongoDB, here is a 5 minute overview of MongoDB from their co-founder and CTO.

### About MongoDB

MongoDB is a document database that lets you store, retrieve, and search collections of "JSON-like" documents.

From the [MongoDB Introduction](https://docs.mongodb.com/manual/introduction/):

A record in MongoDB is a document, which is a data structure composed of field and value pairs. MongoDB documents are similar to JSON objects. The values of fields may include other documents, arrays, and arrays of documents.

![A MongoDB document.](https://docs.mongodb.com/manual/_images/crud-annotated-document.bakedsvg.svg)

### Video

For additional background knowledge about MongoDB (and where it fits in among all the other database varieties), you can watch this presentation:

[Back to Basics 1: Introduction to NoSQL](https://www.mongodb.com/presentations/back-to-basics-webinar-1-introduction-to-nosql)

Note that the video is free, but you may be prompted to enter your name and email to gain access. The first 22 minutes of the video provide an survey of other types of databases, and the remainder focuses on MongoDB specifically. The video has a significant marketing component to it (the presentation is delivered by a "Developer Advocacy" employee of MongoDB) so keep in mind that the criticisms directed at other database technologies are not coming from an unbiased source.

### Installing MongoDB

### MacOS Installation:

We will use HomeBrew to install MongoDB. The following instructions are adapted from the [HomeBrew Installation Instructions](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/#install-mongodb-community-edition-with-homebrew) in the [MongoDB Manual](https://docs.mongodb.com/manual/).

Begin by entering the following commands in the terminal.

brew update  
brew install mongodb

Create a directory for MongoDB to store its data. First, try running:

mkdir -p /data/db

If that fails because your user account doesn't have permission to create that directory, you may have to do:

sudo mkdir -p /data/db  
sudo chown YOURUSERNAME /data/db

(where you should replace the text YOURUSERNAME with the name of your user account).

Finally, check whether MongoDB is successfully installed by running the command:

mongod

You should see a few dozen lines of output on the terminal window as Mongo starts up. Check to make sure you see a line stating:

\[initandlisten\] waiting for connections on port 27017

That confirms that your MongoDB installation is up and running as expected. When you want to shut down your MongoDB service, just enter Ctrl-C in the terminal window to exit.

### Windows Installation:
   - We will be setting up MongoDB as a Service.
1. Go to the MongoDB downloads webpage: [https://www.mongodb.com/try/download/community?tck=docs_server](https://www.mongodb.com/try/download/community?tck=docs_server)
2. Select the "MongoDB Community Server" section.  On the right side of the section there are 3 dropdowns.  Select
   - Version: (current)   (as of this writing the current version is 4.4.2)
   - Platform: Windows
   - Package: msi
3. Once the download is complete, open Windows File Explorer
   - In File Explorer, find the Downloads folder and doubleclick on the msi file to install it. The file should be named something like
   'mongodb-windows-x86_64-4.4.2-signed.msi'
4. Windows Setup Wizard will open.
   1. You will see the message 
       > "The Setup Wizard allows you to change the way MongoDB features are installed on your computer or remove it from your computer. Click Next to continue or Cancel to exit the Setup Wizard."  
       - Click "Next"
   2. End-User License Agreement:
       - Check "I accept the terms in the License Agreement"
       - Click "Next"
   3. Choose Setup Type:
       - Click "Complete"
   4. MongoDB Service Customization:
       - Check "Install MongoDB as a Service"
       - Select "Run service as Network Service user"
       - Service Name: MongoDB
       - Data Directory and Log Directory: You can leave these as the default values.  Make note of the location of the data directory.
       - Default Data Directory should be C:\Program Files\MongoDB\Server\4.4\data\  (the 4.4 will change if your version is not 4.4)
       - Click "Next"
   5. MongoDB Compass:
       - Check "Install MongoDB Compass" (lower left corner)
       - Click "Next"
   6. Ready to install:
       - Click "Install"
   7. User Account Control:
      > "Do you want to allow this app to make changes to your device?"
       - Click "Yes"
   - You may get a popup saying files are in use that need to be updated
       - Select "Close the applications and attempt to restart them"
       - Click "Ok"
   8. Finish Setup:
       - Click "Finish"
   
MongoDB has now been installed as a service.  The MongoDB service will start once installation is finished.  To verify that MongoDB is running, open the Services app and look for MongoDB in the list of services running on your machine.


